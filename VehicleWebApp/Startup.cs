﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VehicleWebApp.Startup))]
namespace VehicleWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
