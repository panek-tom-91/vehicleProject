﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleWebApp.Models
{
    public class Completion
    {
        public Completion()
        {
            this.Vehicles = new HashSet<Vehicle>();
        }
    
        public int CompletionID { get; set; }
        public string EngineStatus { get; set; }
        public string BodyStatus { get; set; }
        public string TiresStatus { get; set; }
        public string Completed { get; set; }
    
        public virtual ICollection<Vehicle> Vehicles { get; set; }
    }
}