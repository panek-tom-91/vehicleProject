﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VehicleWebApp.Models;
using DataAccessLayer;
using System.Data;

namespace VehicleWebApp.Controllers
{
    public class HomeController : Controller
    {
        private VehicleManufacturingEntities db = new VehicleManufacturingEntities();
        
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //private List<Models.Vehicle> vehicleList()
        //{
        //    List<Models.Vehicle> lst = new List<Models.Vehicle>();

        //    lst.Add(new Models.Vehicle {Make = "Honda", Model = "Accord", carYear = 2010, Engine = "6 Cylinder", Body = "Sport", Tires = "Sport"});

        //    return lst;
        //} 

        public ActionResult viewCars()
        {
            ViewBag.Message = "Display your cars.";
            //input code here about displaying model from cars
            //List<Models.Vehicle> lst = new List<Models.Vehicle>();

            //lst.Add(new Models.Vehicle { Make = "Honda", Model = "Accord", carYear = 2010, Engine = "6 Cylinder", Body = "Sport", Tires = "Sport" });

           
            //static data
            //Models.Vehicle vehicle = new Models.Vehicle();

            //vehicle.Make = "Honda";
            //vehicle.Model = "Accord";
            //vehicle.carYear = 2010;
            //vehicle.Engine = "6 cylinder";
            //vehicle.Body = "Sport";
            //vehicle.Tires = "Sport";

            var entities = new VehicleManufacturingEntities();

            //return View("viewCars",vehicle);

            return View(entities.Vehicles.ToList());
        }

        public ActionResult startCar()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult create()
        {



            //try
            //{
            //    if (ModelState.IsValid)
            //    {
            //        db.Vehicles.Add(vehicle);
            //        db.SaveChanges();
            //        return RedirectToAction("Index");
            //    }
            //}
            //catch (DataException /* dex */)
            //{

            //    ModelState.AddModelError("", " Unable to save changes");
            //}
            return View();
        }
    }
}