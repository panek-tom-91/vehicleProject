//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        public int ID { get; set; }
        public int NameID { get; set; }
        public int VehiclesID { get; set; }
        public string Notes { get; set; }
    
        public virtual Name Name { get; set; }
        public virtual Vehicle Vehicle { get; set; }
    }
}
